use super::*;

#[test]

fn is_temperature() {}
// Testing method .to_kelvin()
#[test]
fn from_fahrenheit_to_kelvin() {
    let temp = Temperature::Fahrenheit(5.0);
    assert_eq!(temp.to_kelvin().as_number(), 258.15002);
}

#[test]
fn from_kelvin_to_kelvin() {
    let temp = Temperature::Kelvin(5.0);
    assert_eq!(temp.to_kelvin().as_number(), 5.0);
}
#[test]
fn from_celsius_to_kelvin() {
    let temp = Temperature::Celsius(0.0);
    assert_eq!(temp.to_kelvin().as_number(), 273.0);
}

// Testing method .to_celsius()
#[test]
fn from_fahrenheit_to_celsius() {
    let temp = Temperature::Fahrenheit(41.0);
    assert_eq!(temp.to_celsius().as_number(), 5_f32);
}
#[test]
fn from_kelvin_to_celsius() {
    let temp = Temperature::Kelvin(5.0);
    assert_eq!(temp.to_celsius().as_number(), 278.0);
}
#[test]
fn from_celsius_to_celsius() {
    let temp = Temperature::Celsius(0.0);
    assert_eq!(temp.to_celsius().as_number(), 0.0);
}

// Testing method .to_fahrenheit()
#[test]
fn from_kelvin_to_fahrenheit() {
    let temp = Temperature::Kelvin(278.0);
    assert_eq!(temp.to_fahrenheit().as_number(), 41.0);
}
#[test]
fn from_celsius_to_fahrenheit() {
    let temp = Temperature::Celsius(5.0);
    assert_eq!(temp.to_fahrenheit().as_number(), 41.0);
}
#[test]
fn from_fahrenheit_to_fahrenheit() {
    let temp = Temperature::Fahrenheit(450.0);
    assert_eq!(temp.to_fahrenheit().as_number(), 450.0);
}
