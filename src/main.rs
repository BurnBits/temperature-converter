#[derive(Debug)]
enum Temperature {
    Kelvin(f32),
    Celsius(f32),
    Fahrenheit(f32),
}

impl Temperature {
    fn to_celsius(&self) -> Temperature {
        match self {
            Temperature::Kelvin(tk) => Temperature::Celsius(tk + 273.0),
            Temperature::Fahrenheit(tf) => Temperature::Celsius((tf - 32.0) / 1.8),
            Temperature::Celsius(tc) => Temperature::Celsius(*tc),
        }
    }

    fn to_fahrenheit(&self) -> Temperature {
        match self {
            Temperature::Kelvin(tk) => Temperature::Fahrenheit((tk - 273.0) * 1.8 + 32.0),
            Temperature::Fahrenheit(tf) => Temperature::Fahrenheit(*tf),
            Temperature::Celsius(tc) => Temperature::Fahrenheit(tc * 1.8 + 32.0),
        }
    }

    fn to_kelvin(&self) -> Temperature {
        match self {
            Temperature::Kelvin(tk) => Temperature::Kelvin(*tk),
            Temperature::Fahrenheit(tf) => Temperature::Kelvin((tf + 459.67) * 5.0 / 9.0),
            Temperature::Celsius(tc) => Temperature::Kelvin(tc + 273.0),
        }
    }

    fn as_number(&self) -> f32 {
        match self {
            Temperature::Celsius(tc) => *tc,
            Temperature::Fahrenheit(tf) => *tf,
            Temperature::Kelvin(tk) => *tk,
        }
    }
}

fn main() {
    let temp_f = Temperature::Fahrenheit(73.0);
    let temp_c = Temperature::Celsius(73.0);
    let temp_k = Temperature::Kelvin(73.0);
    println!("{:?}", temp_f.to_celsius());
    println!("{:.2}", temp_c.to_fahrenheit().as_number());
    println!("{:}", temp_k.to_kelvin().as_number());
}

#[cfg(test)]
mod rusty_hook_tests;
